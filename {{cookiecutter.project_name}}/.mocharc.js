"use strict";

module.exports = {
    require: ["ts-node/register", "tsconfig-paths/register"],
    spec: "test/**/*.ts",
    timeout: 2500,
    ui: "bdd",
};
