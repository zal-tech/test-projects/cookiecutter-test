"use strict";

const path = require("path");
const TsConfigPathsPlugin = require("tsconfig-paths-webpack-plugin").TsconfigPathsPlugin;

// Webpack Config Function
module.exports = async function () {
    return {
        mode: "production",
        entry: path.join(__dirname, "src", "index.ts"),
        output: {
            path: path.join(__dirname, "dist"),
            filename: "index.js",
            library: {
                type: "commonjs2",
            },
        },
        module: {
            rules: [
                {
                    test: /\.ts$/,
                    use: ["ts-loader"],
                    include: [path.join(__dirname, "src")],
                },
            ],
        },
        resolve: {
            extensions: [".ts", ".js"],
            plugins: [new TsConfigPathsPlugin({})],
        },
        plugins: [],
        target: "node",
    };
};
