# cookiecutter-test

## Included packages:

### Semantic Release

- [semantic-release](https://github.com/semantic-release/semantic-release): Packaging and release automation.
- [commit-analyzer](https://github.com/semantic-release/commit-analyzer): Analyzes commit messages to determine the type of release.
- [release-notes-generator](https://github.com/semantic-release/release-notes-generator): Generates change log content from commits.
- [changelog](https://github.com/semantic-release/changelog): Updates the changelog file.
- [conventional-changelog-eslint](https://github.com/conventional-changelog/conventional-changelog/tree/master/packages/conventional-changelog-eslint): Changelog/Release notes convention.
- [npm](https://github.com/semantic-release/npm): Bumps the package.json version number according to commit notes, and publishes the new release to a registry.
- [git](https://github.com/semantic-release/git): Commits changelog and version updates back to the repository.
- [gitlab](https://github.com/semantic-release/gitlab): Creates a gitlab release.

### Code Quality

- [eslint](https://github.com/eslint/eslint): Coding patterns and practices.
- [typescript-eslint/parser](https://github.com/typescript-eslint/typescript-eslint/tree/master/packages/parser): Linting for Typescript.
- [typescript-eslint/plugin](https://github.com/typescript-eslint/typescript-eslint/tree/master/packages/eslint-plugin): Eslint plugin for typescript-eslint/parser.
- [eslint-plugin-mocha](https://github.com/lo1tuma/eslint-plugin-mocha): Eslint plugin supporting mocha patterns and practices.
- [prettier](https://github.com/prettier/prettier): File/code formatting tool.

### Testing

- [mocha](https://github.com/mochajs/mocha): Testing framework.
- [chai](https://github.com/chaijs/chai): Expectation library.
- [sinon](https://github.com/sinonjs/sinon): Mocking/Stubbing library.
- Typings for mocha, chai, and sinon/

### Typescript

- [typescript](https://github.com/microsoft/TypeScript): Typesafe superset language of javascript.
- [ts-node](https://github.com/TypeStrong/ts-node): Typescript execution tool.
- Typings for node.

### Utilities

- [tsconfig-paths](https://github.com/dividab/tsconfig-paths): Resolves module loading in tsconfig.json file 'paths' field.
- [tsconfig-paths-webpack-plugin](https://github.com/dividab/tsconfig-paths-webpack-plugin): Allows webpack to resolve module paths defined in tsconfig paths.
- [webpack](https://github.com/webpack/webpack): Code bundling tool.
- [webpack-cli](https://github.com/webpack/webpack-cli): CLI for webpack.
- [ts-loader](https://github.com/TypeStrong/ts-loader): Webpack loader for typescript files.
